using System.Collections;
using UnityEngine;
using PunchPush.Behaviours.Entities;

namespace PunchPush.Game
{
	public class LevelManager : MonoBehaviour
	{
		Box[] _boxes;
		int remainingBoxes;

		static LevelManager _instance;

		void Awake()
		{
			_boxes = FindObjectsOfType<Box>();
			remainingBoxes = _boxes.Length;
			_instance = this;
		}

		public bool IsLevelSolved()
		{
			return remainingBoxes <= 0;
		}

		public static void BoxSolved()
		{
			_instance.remainingBoxes--;
			if (_instance.IsLevelSolved())
			{
				Debug.Log("Level Completed!");
			}
		}
	}
}
