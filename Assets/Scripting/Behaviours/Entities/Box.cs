/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PunchPush;
using PunchPush.Game;
using PunchPush.Behaviours.Common;

namespace PunchPush.Behaviours.Entities
{
	public class Box : Piece
	{
		public enum State
		{
			Idle,
			Pushed,
			Solved,
		}

		public bool Solved => _state == State.Solved;

		[SerializeField]
		Transform _boxMesh;

		[SerializeField]
		AnimationCurve fitCurve;

		[SerializeField]
		float fitAnimationDuration = 1f;

		State _state = State.Idle;

		public bool CanPush(Vector3 direction)
		{
			return _state == State.Idle && IsDirectionFree(direction);
		}

		public IEnumerator Push(Vector3 direction)
		{
			_state = State.Pushed;
			bool solved = WillBeSolved(direction);
			yield return Step(direction);
			_state = solved ? State.Solved : State.Idle;

			if (solved)
			{
				StartCoroutine(Fit());
			}
		}

		bool WillBeSolved(Vector3 direction)
		{
			return Physics.Raycast(Position, direction, out RaycastHit hit, 1f, GameLayers.BoxSlot, QueryTriggerInteraction.Collide);
		}

		IEnumerator Fit()
		{
			if (_state != State.Solved)
			{
				yield return null;
			}

			Vector3 start = _boxMesh.position;
			Vector3 end = _boxMesh.position + Vector3.down * 0.5f;
			float timeStart = Time.time;
			float t = 0f;

			do
			{
				float diff = Time.time - timeStart;
				t = diff / fitAnimationDuration;
				_boxMesh.position = Vector3.Lerp(start, end, fitCurve.Evaluate(t));
				yield return new WaitForEndOfFrame();
			} while (t < 1f);

			_boxMesh.position = end;
			LevelManager.BoxSolved();
		}
	}
}
