/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using PunchPush;
using PunchPush.Behaviours.Common;
using PunchPush.GameDevSettings;

namespace PunchPush.Behaviours.Entities
{
	public class Protagonist : Piece
	{
		[SerializeField] State _state;
		PlayerActions _playerActions;

		public enum State
		{
			Idle = 0,
			Walking,
			Turning,
		}

#region MonoBehaviour Callbacks

		protected override void Awake()
		{
			base.Awake();
			_playerActions = new PlayerActions();
			_playerActions.Gameplay.StepForward.performed += ctx => StartCoroutine(OnStep(ctx, Forward));
			_playerActions.Gameplay.StepBackward.performed += ctx => StartCoroutine(OnStep(ctx, Backward));
			_playerActions.Gameplay.Rotate.performed += ctx => StartCoroutine(OnRotate(ctx));

			_state = State.Idle;
		}

		void OnEnable()
		{
			_playerActions.Gameplay.Enable();
		}

		void OnDisable()
		{
			_playerActions.Gameplay.Disable();
		}

#endregion

		IEnumerator OnStep(InputAction.CallbackContext context, Vector3 direction)
		{
			bool canWalk = IsDirectionFree(direction);
			Transform boxTr = CheckBox(direction);
			Box box = null;

			if (canWalk && boxTr)
			{
				box = boxTr.GetComponent<Box>();
				canWalk = box.CanPush(direction);
			}
		
			if (canWalk)
			{
				State previous_state = _state;
				if (_state == State.Idle)
				{
					_state = State.Walking;
					if (box)
					{
						StartCoroutine(box.Push(direction));
					}
					yield return Step(direction);
				}
				_state = previous_state;
			}
		
			yield return null;
		}

		IEnumerator OnRotate(InputAction.CallbackContext context)
		{
			State previous_state = _state;
			if (_state == State.Idle)
			{
				_state = State.Turning;
				yield return Rotate(context.ReadValue<float>());
			}
			_state = previous_state;
			yield return null;
		}

		Transform CheckBox(Vector3 direction)
		{
			Transform tr = QueryNeighbours.Query(Position, direction, GameLayers.Box);
			return tr;
		}
	}
}
