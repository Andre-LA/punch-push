/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using System;
using System.Collections;
using UnityEngine;

namespace PunchPush.Behaviours.Common
{
	public class Movement : MonoBehaviour
	{
		Transform tr;

		const float duration = 0.2f;
		bool isWalking;

		public IEnumerator Step(Vector3 direction)
		{
			yield return StepLerp(direction);
		}

		public IEnumerator Rotate(float dir)
		{
			yield return RotateLerp(dir);
		}

		IEnumerator RotateLerp(float dir)
		{
			Quaternion start = tr.localRotation;
			Quaternion end = Quaternion.Euler(tr.localEulerAngles + Vector3.up * 90f * dir);
			float startTime = Time.time;

			while (TLerp(out var val, start, end, startTime, Quaternion.Lerp) < 1f)
			{
				tr.localRotation = val;
				yield return new WaitForEndOfFrame();
			}

			tr.localRotation = end;
		}

		IEnumerator StepLerp(Vector3 direction)
		{
			Vector3 start = tr.localPosition;
			Vector3 end = start + direction;
			float startTime = Time.time;

			while (TLerp(out var val, start, end, startTime, Vector3.Lerp) < 1f)
			{
				tr.localPosition = val;
				yield return new WaitForEndOfFrame();
			}

			tr.localPosition = end;
		}

		float TLerp<T>(out T target, T start, T end, float startTime, Func<T, T, float, T> lerpFn)
		{
			float t = (Time.time - startTime) / duration;
			target = lerpFn(start, end, t);
			return t;
		}

		void Awake()
		{
			tr = GetComponent<Transform>();
		}
	}
}