/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using System.Collections;
using UnityEngine;

namespace PunchPush.Behaviours.Common
{
	[RequireComponent(typeof(Movement))]
	public class Piece : MonoBehaviour
	{
		[SerializeField] LayerMask blockers;
		Transform _tr;
		Movement _movement;
	
#region Properties
		public Vector3 Forward => _tr.forward;
		public Vector3 Right => _tr.right;
		public Vector3 Backward => -_tr.forward;
		public Vector3 Left => -_tr.right;
		public Vector3 Position => _tr.position;
#endregion

#region Unity Callbacks
		protected virtual void Awake()
		{
			_tr = GetComponent<Transform>();
			_movement = GetComponent<Movement>();
		}
#endregion

#region  methods

		protected bool IsDirectionFree(Vector3 direction)
		{
			return !QueryNeighbours.Query(_tr.position, direction, blockers);
		}

		protected IEnumerator Step(Vector3 direction)
		{
			yield return _movement.Step(direction);
		}

		protected IEnumerator Rotate(float dir)
		{
			yield return _movement.Rotate(dir);
		}

#endregion
	}
}
