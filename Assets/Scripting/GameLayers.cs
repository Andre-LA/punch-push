/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PunchPush
{
	public static class GameLayers
	{
		public static LayerMask Wall => LayerMask.GetMask("Wall");
		public static LayerMask Actor => LayerMask.GetMask("Actor");
		public static LayerMask Box => LayerMask.GetMask("Box");
		public static LayerMask BoxSlot => LayerMask.GetMask("BoxSlot");
	}
}
