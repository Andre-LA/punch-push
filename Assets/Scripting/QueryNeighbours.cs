/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using UnityEngine;

namespace PunchPush
{
	public class QueryNeighbours
	{
		public static Transform Query(Vector3 origin, Vector3 direction, LayerMask layerMask)
		{
			if (Physics.Raycast(origin, direction, out RaycastHit hit, 1f, layerMask))
			{
				return hit.transform;
			}

			return null;
		}
	}
}
