/*
Copyright (c) 2023-present André Luiz Alvares
Nene is licensed under the MPL-2.0 license.
Please refer to the LICENSE file for details
SPDX-License-Identifier: MPL-2.0
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PunchPush
{
	public static class GameTags
	{
		public static string Player => "Player";
		public static string Box => "Box";
		public static string BoxSlot => "BoxSlot";
	}
}
